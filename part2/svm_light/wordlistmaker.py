__author__ = 'shwetamouli'
import pickle

inputfile = "/home/shwetamouli/PycharmProjects/SENTIMENT_testSet.txt"
temp = "sentitemp"
outputfile = "senti_bow"
picklef = "sentipickle"

f = open(inputfile, "r")
f2 = open(temp, "w")
bag_of_words = dict()
for line in f:
    line = line.split(' ')
    label = line.pop(0)
    #print(label)
    if label == "NEG":
        index = 1
    elif label == "POS":
        index = 0

    for word in line:
        #if word is not "HAM" and word is not "SPAM":
        word = word.lower()
        #word = ''.join(e for e in word if e.isalnum())
        if word is not '\n' and word is not ' ' and word is not '\r':
            try:
                bag_of_words[word][index] += 1
                #count_by_label[label_index[words_in_line[0]][0]] += 1
            except KeyError:
                bag_of_words[word] = [1, 1]
                #unique_by_label[label_index[words_in_line[0]][0]] += 1
            #count_by_label[label_index[words_in_line[0]][0]] += 1

for key in bag_of_words:
    if key is not '\r':
        f2.write(key+"\n")

f2.close()
f.close()

f1 = open(temp, "r")
f2 = open(outputfile, "w")

for line in f1:
    if (line == "\n") or (line == " ") or(line == "\t") or (line == "\r"):
        continue
    else:
        f2.write(line)

pickle.dump(bag_of_words, open(picklef, "wb"))