__author__ = 'shwetamouli'
inputfile = "svm.predictions.spam"
outputfile = "spam.svm.out"
f2 = open(outputfile, "w")

with open(inputfile, "r") as f1:
    for line in f1:
        if float(line.rstrip("\n")) >= 1:
            f2.write("HAM\n")
        else:
            f2.write("SPAM\n")

