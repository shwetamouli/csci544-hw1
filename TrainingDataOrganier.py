import os
import re

path = '/home/shwetamouli/Downloads/SENTIMENT_test'
data = {}
outputfile = "/home/shwetamouli/PycharmProjects/SENTIMENT_testSet.txt"
f = open(outputfile, 'w')
labels = list()
filelist = os.listdir(path)
filelist.sort()
for dir_entry in filelist:
    dir_entry_path = os.path.join(path, dir_entry)
    if os.path.isfile(dir_entry_path):
        with open(dir_entry_path, 'r', encoding="iso-8859-15") as curr_file:
            file_content = curr_file.read()
            file_content = file_content.replace('\n', ' ')
            file_content = file_content.replace('Subject:', '')
            file_content = re.sub('\W+', ' ', file_content)
            #print(file_content)
            f.write(dir_entry.split('.')[0]+" "+file_content+"\n")
            #label = file_content.split(' ')[0]
            if labels.__contains__(dir_entry.split('.')[0]) is False:
                labels.append(dir_entry.split('.')[0])
            #data[dir_entry] = my_file.read()
#print(file_content)
f.close()
#print(data)
