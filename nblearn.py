import pickle
import math
import sys

training_file_path = sys.argv[1]
training_file = open(training_file_path, 'r')
nb_model_path = sys.argv[2]
bag_of_words = dict()
#print(training_file.readline())

count_by_label = list()
count_by_label.append(0)
count_by_label.append(0)

unique_by_label = list()
unique_by_label.append(0)
unique_by_label.append(0)

label_count = list()
label_count.append(0)
label_count.append(0)

label_index = dict()
index = 0

for line in training_file:
    #print(line)
    words_in_line = line.split(' ')
    # if words_in_line[0] == "HAM":
    #     anchor = 0
    #     label_count[anchor] += 1
    # elif words_in_line[0] == "SPAM":
    #     anchor = 1
    #     label_count[anchor] += 1
    labels = label_index.keys()
    if words_in_line[0] in labels:
        label_index[words_in_line[0]][1] += 1
    else:
        label_index[words_in_line[0]] = [index, 1]
        index += 1

    for word in words_in_line:
        #if word is not "HAM" and word is not "SPAM":
        word = word.lower()
        word = ''.join(e for e in word if e.isalnum())
        try:
            bag_of_words[word][label_index[words_in_line[0]][0]] += 1
            count_by_label[label_index[words_in_line[0]][0]] += 1
        except KeyError:
            bag_of_words[word] = [1, 1]
            unique_by_label[label_index[words_in_line[0]][0]] += 1
            count_by_label[label_index[words_in_line[0]][0]] += 1

#smoothing denominator
unique_words = 0
for uniquelabel in unique_by_label:
    unique_words += uniquelabel
for countlabel in count_by_label:
    countlabel += unique_words

total_words = 0
for label in label_index:
    total_words += label_index[label][1]

prob_by_label = dict()
for label in label_index:
    prob_by_label[label_index[label][0]] = math.log(label_index[label][1]/total_words)
#prob_label1 = label_count[0]/(label_count[1]+label_count[0])
#prob_label2 = label_count[1]/(label_count[1]+label_count[0])

word_prob = dict()

for key in bag_of_words.keys():
    probbylabel = dict()
    for label in label_index:
        # try:
        #     word_prob[key].append([label, label_index[label][0], math.log(bag_of_words[key][label_index[label][0]]/count_by_label[label_index[label][0]])])
        # except KeyError:
        #     word_prob[key] = list()
        #     word_prob[key].append([label, label_index[label][0], math.log(bag_of_words[key][label_index[label][0]]/count_by_label[label_index[label][0]])])
        probbylabel[label_index[label][0]] = math.log(bag_of_words[key][label_index[label][0]]/count_by_label[label_index[label][0]])
    word_prob[key] = probbylabel

datalist = [word_prob, prob_by_label, label_index]

#print(count_by_label, unique_by_label, label_count, label_index, prob_by_label)
pickle.dump(datalist, open(nb_model_path, "wb"))
