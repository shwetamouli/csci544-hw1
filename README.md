# README #
I used some help from StackOverflow to read files from directory in python.

1. In part 1, the scores I calculated using the dev set are as follows:
SPAM- precision: 0.9444
SPAM- recall: 0.9834
SPAM- F-score: 0.9635

HAM- precision: 0.9939
HAM- recall: 0.979
HAM- F-score: 0.9864

POS- precision: 0.9177
POS- recall: 0.8035
POS- F-score: 0.8568

NEG- precision: 0.6853
NEG- recall: 0.856
NEG- F-score: 0.7612

2. Using SVM and MegaM, and the dev sets, these are the numbers I got:
a. SVM:
SPAM- precision: 0.6400
SPAM- recall: 0.9945
SPAM- F-score: 0.7788

HAM- precision: 0.9976
HAM- recall: 0.837
HAM- F-score: 0.9102

POS- precision: 0.9241
POS- recall: 0.87
POS- F-score: 0.8962

NEG- precision: 0.768
NEG- recall: 0.857
NEG- F-score: 0.81

b. MegaM
SPAM- precision: 0.969
SPAM- recall: 0.994
SPAM- F-score: 0.9813

HAM- precision: 0.989
HAM- recall: 0.991
HAM- F-score: 0.9899

POS- precision: 0.6963
POS- recall: 0.626
POS- F-score: 0.6593

NEG- precision: 0.6603
NEG- recall: 0.727
NEG- F-score: 0.692

3. I tried this with different chunks of the training data. It showed varied results but with noticeably lower accuracy in two out of three cases. I think this might have to do with the exact nature of the training data - if it is very specific to the data that we're testing, the accuracy might improve. That being said, the learning and classifying were slightly faster with the reduced training set size.
