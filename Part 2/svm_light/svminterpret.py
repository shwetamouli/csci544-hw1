__author__ = 'shwetamouli'
inputfile = "sentiment.svm.predict"
outputfile = "sentiment.svm.out"
f2 = open(outputfile, "w")

with open(inputfile, "r") as f1:
    for line in f1:
        if float(line.rstrip("\n")) >= 0:
            f2.write("POS\n")
        else:
            f2.write("NEG\n")

