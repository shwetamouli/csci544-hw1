__author__ = 'shwetamouli'
import pickle

inputfile = "/home/shwetamouli/PycharmProjects/SPAM_testSet.txt"
bow = "spam_bow"
outputfile = "spam_test1.dat"
bag_of_words = pickle.load(open("spampickle", "rb"))


f = open(inputfile, "r")
f2 = open(outputfile, "w")

bowdict = dict()
bindex = 1
with open(bow, "r") as bowf:
    for word in bowf:
        bowdict[word.rstrip('\n')] = bindex
        bindex += 1

for line in f:
    line = line.split(' ')
    label = line.pop(0)
    #print(label)
    if label == "HAM":
        index = 1
        pindex = "-1"
    elif label == "SPAM":
        index = 0
        pindex = "1"
    #print(line)
    pindex = "+1"
    linedict = dict()
    for word in line:
        word = word.lower()
        #print(word)
        #for num, wordline in enumerate(bow, 1):
        #print(word, wordline)
        #print(bowdict.keys())
        if word in bowdict.keys():
            try:
                linedict[bowdict[word]] += 1
            except KeyError:
                linedict[bowdict[word]] = 1
    f2.write(pindex + " ")
    for key in sorted(linedict):
        f2.write(str(key)+":"+str(linedict[key])+" ")
    f2.write("\n")
    #print(linedict)

